import os

class Config(object):
	DEBUG = False
	SQLALCHEMY_DATABASE_URI = os.getenv('STREAM_DATABASE_URL')
	CMDCTRL_DATABASE_URL = os.getenv('CMDCTRL_DATABASE_URL')
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SQLALCHEMY_POOL_SIZE = 25
	SQLALCHEMY_MAX_OVERFLOW = 10
	SECRET_KEY = os.environ['SECRET_KEY']
	GET_ONE_PROSUMER_URI = os.environ.get('GET_ONE_PROSUMER_URI')
	SOCKETIO_MESSAGE_QUEUE = os.environ.get(
		'SOCKETIO_MESSAGE_QUEUE',
		'redis://',
	)
	CELERY_CONFIG = {}
	OFFLINE_SLEEP_TIME = 20
	# SUBSCRIBE_TO = literal_eval(os.getenv('SUBSCRIBE_TO'))

	@staticmethod
	def init_app(app):
		pass


class Development(Config):
	DEBUG = True

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)

	@staticmethod
	def init_app(app):
		pass


class Testing(Config):
	"""TODO"""
	CELERY_CONFIG = {'CELERY_ALWAYS_EAGER': True}
	SOCKETIO_MESSAGE_QUEUE = None

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Staging(Config):
	"""TODO"""
	OFFLINE_SLEEP_TIME = 3600

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Production(Config):
	RESTPLUS_MASK_SWAGGER = True

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


config = {
	'Development': Development,
	'Testing': Testing,
	'Production': Production,
	'Staging': Staging,

	'default': Development,


}
