import os

from celery import Celery
from flask import Flask
from flask_cors import CORS
from flask_restplus import Api
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from pymongo import MongoClient

from config import config
from logger import logger
api = Api(doc='/doc/')
socketio = SocketIO(logger=True)
db = SQLAlchemy()
# logger.info("SocketIO Client Manager: [{}]".format(socketio.server_options['client_manager'].__class__.__name__))


def create_api(config_name, main=True):
	app = Flask(__name__)

	try:
		init_config = config[config_name]()
		logger.info('Running in {} Mode'.format(init_config))
	except KeyError:
		raise
	except Exception as e:
		logger.exception(e)
		# For unforseen exceptions
		raise

	config_object = config.get(config_name)
	app.config.from_object(config_object)
	cors = CORS(app, resources={r"/*":{"origins":"*"}})
	# import logging
	# logging.getLogger('flask_cors').level = logging.DEBUG

	from api import events
	from api.controllers import cmdctrl_api as ns1
	api.add_namespace(ns1, path='/cmdctrl')
	api.init_app(app)
	# db.init_app(app)
	if main:
		# Initialize socketio server and attach it to the message queue, so
		# that everything works even when there are multiple servers or
		# additional processes such as Celery workers wanting to access
		# Socket.IO
		app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('STREAM_DATABASE_URL')
		socketio.init_app(
			app,
			message_queue=app.config['SOCKETIO_MESSAGE_QUEUE'],
			async_mode='eventlet'
		)
		logger.info("SocketIO Client Manager: [{}]".format(socketio.server_options['client_manager'].__class__.__name__))
	else:
		# Initialize socketio to emit events through through the message queue
		# Note that since Celery does not use eventlet, we have to be explicit
		# in setting the async mode to not use it.
		# from stream.models import DeclarativeBase, engine
		# DeclarativeBase.metadata.create_all(engine)
		app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('CMDCTRL_DATABASE_URL')
		socketio.init_app(
			None,
			message_queue=app.config['SOCKETIO_MESSAGE_QUEUE'],
			async_mode='threading'
		)
		logger.info("SocketIO Client Manager: [{}]".format(socketio.server_options['client_manager'].__class__.__name__))
	# celery.conf.update(config[config_name].CELERY_CONFIG)

	db.init_app(app)
	with app.app_context():
		# db.drop_all()
		db.create_all()
	# from api.controllers import auth_api as ns1
	# api.add_namespace(ns1, path='/auth')


	return app
