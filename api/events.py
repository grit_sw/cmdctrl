"""Handle socket communication via socketio"""
from eventlet import monkey_patch
from flask_socketio import join_room, leave_room

from api import socketio
from logger import logger

monkey_patch()


@socketio.on('my_facility')
def join_facility(facility_data):
	# leave the temporary room once you have performed the operation requested in that room
	join_room('facility_request_room')
	logger.info('Facilities obtained.')
	for facility in facility_data['data']:
		join_room(facility['facility_id'])
		logger.info('Successfully listening to messages in facility {}.'.format(facility['facility_id']))
	# leave the temporary room once you have performed the operation requested in that room
	leave_room('facility_request_room')


@socketio.on('register')
def register(data):
	logger.info('Registering User')
	role_id = int(data['data']['role_id'])
	group_id = data['data']['group_id']
	configuration_ids = data['data']['configuration_ids']
	if role_id == 5:
		# leave the temporary room once you have performed the operation requested in that room
		leave_room('handshake_room')
		logger.info('Handshake complete')
		stream_room = 'all-messages'  # TODO: make this room unique for all users.
		join_room(stream_room)
		socketio.emit('connection_response', 'Receiving messages all messages', room=stream_room)
		logger.info('Successfully listening to all messages.')

	if role_id == 4:
		# leave the temporary room once you have performed the operation requested in that room
		leave_room('handshake_room')
		logger.info('Handshake complete')
		logger.info('Requesting facilities from new management admin.')
		join_room('facility_request_room')
		for configuration_id in configuration_ids:
			join_room(configuration_id)
		socketio.emit('get_facilities', room='facility_request_room')

	if role_id < 4:
		# Client admins and user admins only have messages sent to their group IDs
		# leave the temporary room once you have performed the operation requested in that room
		leave_room('handshake_room')
		logger.info('Handshake complete')
		join_room(group_id)
		for configuration_id in configuration_ids:
			join_room(configuration_id)
		socketio.emit('connection_response', 'Receiving messages from group {}'.format(group_id), room=group_id)
	# leave_room('handshake_room')
	# logger.info('Handshake complete')


@socketio.on('handshake_init')
def handshake():
	logger.info('Initiating handshake')
	# create a temporary room so that other connected clients are not forced to respond to facility request
	# todo: let frontend application create a random signature to be used for communicating with the backend for registration.
	# todo: this will ensure one-to-one communication between the frontend and backend during registration.
	join_room('handshake_room')
	socketio.emit('init_registration', room='handshake_room')
	# join_room('all-messages')


@socketio.on('connect')
def connect():
	logger.info('Socket successfully connected')


def push_message(event, state_message, room_info):
	"""Push the model to all connected Socket.IO clients."""
	facility_id = room_info.get('facility_id')
	admins = room_info.get('admins')
	configurations = room_info.get('configurations')
	logger.info('Emiting messages to concerned rooms')

	socketio.emit(event, {'message': state_message}, room='all-messages')
	if facility_id:
		socketio.emit(event, {'message': state_message}, room=facility_id)
	if admins:
		for admin_room in admins:
			socketio.emit(event, {'message': state_message}, room=admin_room)

	if configurations:
		for configuration_room in configurations:
			socketio.emit(event, {'message': state_message}, room=configuration_room)

	logger.info('State message {} sent'.format(state_message))
	logger.info('Room Info {}'.format(room_info))
