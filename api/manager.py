from api.models import Prosumer, Probe
from logger import logger
# from flask import Response, request
from flask import request

class StreamManager(object):

	def new(self, data):
		response = {}
		prosumer_id = data.get('prosumer_id')
		if prosumer_id:
			prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		else:
			response['success'] = False
			response['data'] = []
			return response, 400
		if not prosumer:
			new_prosumer = Prosumer.create(**data)
			response['success'] = True
			response['data'] = new_prosumer.get_data()
			Prosumer.commit()
			return response, 201
		else:
			response['success'] = False
			response['message'] = 'Prosumer already exists.'
			Prosumer.commit()
			return response, 409

	def edit(self, data):
		response = {}
		prosumer_id = data.get('prosumer_id')
		del data['prosumer_id']
		if prosumer_id:
			prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		else:
			response['success'] = False
			response['data'] = []
			return response, 400
		if not prosumer:
			response['success'] = False
			response['message'] = 'Prosumer not found.'
			return response, 404
		new_prosumer = prosumer.edit(**data)
		response['success'] = True
		response['data'] = new_prosumer.get_data()
		Prosumer.commit()
		return response, 201

	def ignore_prosumer(self, prosumer_id):
		response = {}
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if prosumer:
			success = prosumer.ignore()
			response['success'] = success
			return response, 200 if success else 501

		if not prosumer:
			response['success'] = False
			response['message'] = 'Prosumer not found.'
			return response, 404

	def update_user(self, data):
		response = {}
		prosumer_id = data['prosumer_id']
		del data['prosumer_id']
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if prosumer:
			prosumer.set_user(**data)
			response['success'] = True
			return response, 200
		response['success'] = False
		response['message'] = 'Prosumer not found.'
		Prosumer.commit()
		return response, 404

	def record_power_state(self, data):
		response = {}
		prosumer_id = data['prosumer_id']
		configuration_id = data['configuration_id']
		probe = [data['probe_id']]
		del data['prosumer_id']
		del data['configuration_id']
		prosumer = Prosumer.get_one_or_create(prosumer_id, configuration_id, probe)
		if prosumer.ignored:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 404
		# if not prosumer:
		# 	prosumer = Prosumer.get_one_or_create(prosumer_id, probe)
		prosumer = prosumer.record_power_state(**data)
		response['success'] = True
		response['data'] = prosumer.get_data()
		Prosumer.commit()
		return response, 201

	def record_down_state(self, data):
		response = {}
		logger.info(data)
		try:
			prosumer_id = data['prosumer_id']
		except:
			logger.info(data)
			raise
		configuration_id = data['configuration_id']
		probe_ids = data['probe_ids']
		del data['prosumer_id']
		del data['probe_ids']
		del data['configuration_id']
		prosumer = Prosumer.get_one_or_create(prosumer_id, configuration_id, probe_ids)
		if prosumer.ignored:
			response['success'] = False
			response['message'] = 'Meter not found'
			Prosumer.commit()
			return response, 404
		# if not prosumer:
		# 	prosumer = Prosumer.get_one_or_create(prosumer_id, probe)
		prosumer = prosumer.record_down_state(**data)
		response['success'] = True
		response['data'] = prosumer.get_data()
		Prosumer.commit()
		return response, 201

	def record_probe_down_state(self, data):
		response = {}
		probe_id = data['probe_id']
		del data['probe_id']
		probe = Probe.get_or_create(probe_id)
		probe = probe.record_down_state(**data)
		response['success'] = True
		response['data'] = probe.get_data()
		Prosumer.commit()
		return response, 201

	def delete_prosumer(self, prosumer_id):
		response = {}
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if not prosumer:
			response['success'] = False
			response['message'] = 'Prosumer not found.'
			return response, 404
		prosumer.delete()
		response['success'] = True
		response['message'] = 'Prosumer deleted.'
		return response, 201

	def all_prosumers(self):
		response = {}
		# stream = request.args.get('stream', False, type=bool)
		role_id = request.cookies.get('role_id')
		data = []
		try:
			role_id = int(role_id)
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		if role_id >= 5:
			try:
				data = Prosumer.all_prosumers()
				# if stream:
				# 	def generate():
				# 		for d in data:
				# 			yield d
				# 		return Response(generate(), mimetype='text/plain'), 206
				# # create stream response
				# response['success'] = True
				# response['data'] = data
				# return response, 201

			except Exception as e:
				logger.exception(e)
				response = {}
				response['success'] = False
				response['message'] = 'Internal Server Error'
				return response, 500
		if role_id in {3, 4}:
			configuration_ids = request.cookies.get('configuration_ids')
			if not isinstance(configuration_ids, list):
				response['success'] = False
				response['message'] = 'Unauthourised.'
				return response, 403
			data = Prosumer.by_configs(configuration_ids)
		if role_id <= 2:
			user_group_id = request.cookies.get('group_id')
			if not user_group_id:
				response['success'] = False
				response['message'] = 'Unauthourised.'
				return response, 403
			data = Prosumer.by_user_group(user_group_id)
		if not data:
			response['success'] = False
			response['message'] = 'Prosumers not found.'
			Prosumer.commit()
			return response, 404
		else:
			response['success'] = True
			response['data'] = data
			Prosumer.commit()
			return response, 200
			# todo: stream response
			# def stream_prosumers():
			# 	for d in data:
			# 		yield d
			# response = Response(stream_prosumers(), mimetype='text/plain')
			# return response, 206
