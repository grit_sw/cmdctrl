from flask import request
from flask_restplus import Namespace, Resource, fields

from api import api
from api.manager import StreamManager

cmdctrl_api = Namespace('cmdctrl', description='Api for Command and Control.')
new = cmdctrl_api.model('New Prosumer', {
	'prosumer_name': fields.String(required=True, description='Prosumer name.'),
	'prosumer_id': fields.String(required=True, description='Prosumer ID.'),
	'prosumer_url': fields.String(required=True, description='Prosumer URL.'),
	'user_group_id': fields.String(required=True, description='User group ID (The user of the prosumer).'),
	'user_full_name': fields.String(required=True, description='User\'s full name.'),
	'facility_id': fields.String(required=True, description='The facility ID the prosumer is installed at.'),
	'facility_name': fields.String(required=True, description='The name if the facility the prosumer is installed at.'),
	'prosumer_state': fields.String(required=True, description='The state of the prosumer.'),
	'client_admins': fields.List(fields.String(required=True, description='The ID of one client admin associated with the prosumer'), 
		description='List of client admins associated with the prosumer.'),
	'probe_names': fields.List(fields.String(required=True, description='The ID of one probe associated with the prosumer'), description='The list of probes that are associated with this prosumer.'),
})

user = cmdctrl_api.model('Update User', {
	'prosumer_id': fields.String(required=True, description='Prosumer ID.'),
	'user_group_id': fields.String(required=True, description='User group ID (The user of the prosumer).'),
	'user_full_name': fields.String(required=True, description='User\'s full name.'),
})

facility = cmdctrl_api.model('Update Facility', {
	'prosumer_id': fields.String(required=True, description='Prosumer ID.'),
	'facility_id': fields.String(required=True, description='The facility ID the prosumer is installed at.'),
	'facility_name': fields.String(required=True, description='The name if the facility the prosumer is installed at.'),
})

power_state = cmdctrl_api.model('Power State Update', {
	'probe_id': fields.String(required=True, description='Probe ID.'),
	'probe_state': fields.String(required=True, description='tProbe State.'),
	'prosumer_id': fields.String(required=True, description='Prosumer ID.'),
	'prosumer_state': fields.String(required=True, description='Prosumer State.'),
	'prosumer_name': fields.String(required=True, description='Prosumer name.'),
	'measured_power': fields.Float(required=False, description='The measured power.'),
	'measured_energy': fields.Float(required=False, description='The measured energy.'),
	'energy_today': fields.Float(required=False, description='The measured energy today.'),
	'time_today': fields.Float(required=False, description='The time duration the prosumer has been ON for.'),
	'last_message': fields.String(required=True, description='The time the measurement was taken.'),
	'from_buffer': fields.Integer(required=True, description='Is this message a buffered message.'),
})

down_state = cmdctrl_api.model('Down State Update', {
	'prosumer_id': fields.String(required=True, description='Prosumer ID.'),
	'probe_ids': fields.List(fields.String(required=True), description='Probe IDs associated with the prosumer.'),
	'prosumer_state': fields.String(required=True, description='Prosumer State.'),
	'user_full_name': fields.String(required=False, description='Prosumer User name.'),
	'user_group_id': fields.String(required=False, description='Prosumer user group ID.'),
	'last_message': fields.String(required=True, description='The time the measurement was taken.'),
})


probe_down_state = cmdctrl_api.model('Probe Down State Update', {
	'probe_id': fields.String(required=True, description='Probe ID.'),
	'probe_state': fields.String(required=True, description='Probe State.'),
	'last_message': fields.String(required=True, description='The time the measurement was taken.'),
})


@cmdctrl_api.route('/new/')
class New(Resource):
	"""
		Api for creating new prosumers.
	"""

	@cmdctrl_api.expect(new)
	def post(self):
		"""
			HTTP method for creating new prosumers.
			@param: payload: data for the update
			@returns: response and status code
		"""
		payload = api.payload
		manager = StreamManager()
		role_id = request.cookies.get('role_id')
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		print('payload', payload)
		resp, code = manager.new(payload)
		return resp, code


@cmdctrl_api.route('/edit/')
class Edit(Resource):
	"""
		Api for editing one prosumer.
	"""

	@cmdctrl_api.expect(new)
	def post(self):
		"""
			HTTP method for editing one prosumer.
			@param: payload: data for the update
			@returns: response and status code
		"""
		payload = api.payload
		manager = StreamManager()
		role_id = request.cookies.get('role_id')
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.edit(payload)
		return resp, code


@cmdctrl_api.route('/update-user/')
class UpdateUser(Resource):
	"""
		Api for updating the user of one prosumer.
	"""

	@cmdctrl_api.expect(user)
	def post(self):
		"""
			HTTP method for updating the user of one prosumer.
			@param: payload: data for the update
			@returns: response and status code
		"""
		payload = api.payload or request.values.to_dict()
		manager = StreamManager()
		role_id = int(request.cookies.get('role_id'))
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.update_user(payload)
		return resp, code


@cmdctrl_api.route('/record-power-state/')
class RecordPowerState(Resource):
	"""
		Api for recording the power state of one prosumer.
	"""

	@cmdctrl_api.expect(power_state)
	def post(self):
		"""
			HTTP method for recording the power state of one prosumer.
			@param: payload: data for the update
			@returns: response and status code
		"""
		payload = api.payload or request.values.to_dict()
		manager = StreamManager()
		role_id = int(request.cookies.get('role_id'))
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.record_power_state(payload)
		return resp, code


@cmdctrl_api.route('/record-down-state/')
class RecordDownState(Resource):
	"""
		Api for recording the down state of one prosumer.
	"""

	@cmdctrl_api.expect(down_state)
	def post(self):
		"""
			HTTP method for recording the down state of one prosumer.
			@param: payload: data for the update
			@returns: response and status code
		"""
		payload = api.payload or request.values.to_dict()
		manager = StreamManager()
		role_id = int(request.cookies.get('role_id'))
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.record_down_state(payload)
		return resp, code


@cmdctrl_api.route('/record-probe-down-state/')
class RecordProbeDownState(Resource):
	"""
		Api for recording the down state of one prosumer.
	"""

	@cmdctrl_api.expect(probe_down_state)
	def post(self):
		"""
			HTTP method for recording the down state of one prosumer.
			@param: payload: data for the update
			@returns: response and status code
		"""
		payload = api.payload or request.values.to_dict()
		manager = StreamManager()
		role_id = int(request.cookies.get('role_id'))
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.record_probe_down_state(payload)
		return resp, code


@cmdctrl_api.route('/ignore-prosumer/<string:prosumer_id>/')
class IgnoreProsumer(Resource):
	"""
		Api for recording the power state of one prosumer.
	"""

	def post(self, prosumer_id):
		"""
			HTTP method to stop monitoring the prosumer.
			@param: payload: data for the update
			@returns: response and status code
		"""
		manager = StreamManager()
		role_id = request.cookies.get('role_id')
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.ignore_prosumer(prosumer_id)
		return resp, code


@cmdctrl_api.route('/all-prosumers/')
class AllProsumers(Resource):
	"""
		Api for getting all prosumers.
	"""

	def get(self):
		"""
			HTTP method for getting all prosumers.
			@param: payload: data for the update
			@returns: response and status code
		"""
		manager = StreamManager()
		role_id = int(request.cookies.get('role_id'))
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.all_prosumers()
		return resp, code


@cmdctrl_api.route('/delete/<string:prosumer_id>/')
class DeleteProsumer(Resource):
	"""
		Api for deleting a prosumer.
	"""

	def delete(self, prosumer_id):
		"""
			HTTP method for deleting a prosumer.
			@param: payload: data for the update
			@returns: response and status code
		"""
		manager = StreamManager()
		role_id = int(request.cookies.get('role_id'))
		if role_id < 5:
			response = {}
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		resp, code = manager.delete_prosumer(prosumer_id)
		return resp, code
