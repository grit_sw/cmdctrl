from stream import ProsumerMonitor, OfflineChecker
from api import celery


# @celery.task
def start_cmdctrl():
	from api.celery_aux import app

	monitor = ProsumerMonitor(
	    bootstrap_servers='localhost',
	    schema_registry='http://localhost:8081',
	    group_id='prosumer_monitor',
	    topics=['probe-stream', 'prosumer_update'],
	)
	with app.app_context():
		monitor.run()

# @celery.task
def offline_job():
	from api.celery_aux import app

	checker = OfflineChecker()
	with app.app_context():
		checker.run()
