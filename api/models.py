from itertools import chain

import arrow
from sqlalchemy_utils import ArrowType

from api import db
from logger import logger


def commit():
	try:
		db.session.commit()
	except:
		db.session.rollback()

def add(item):
	db.session.add(item)
	return item

def add_and_commit(item):
	db.session.add(item)
	commit()
	db.session.refresh(item)
	return item

class Base(db.Model):
	__abstract__ = True
	id_ = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(ArrowType, default=db.func.current_timestamp())
	date_modified = db.Column(ArrowType, default=db.func.current_timestamp(),
								onupdate=arrow.now('Africa/Lagos'))

probe_prosumers = db.Table('probe_prosumers',
	db.Column('probe_id', db.String(100), db.ForeignKey('probe.probe_id')),
	db.Column('prosumer_id', db.String(100), db.ForeignKey('prosumer.prosumer_id')),
)

prosumer_admins = db.Table('prosumer_admins',
	db.Column('admin_id', db.String(100), db.ForeignKey('admin.admin_id')),
	db.Column('prosumer_id', db.String(100), db.ForeignKey('prosumer.prosumer_id')),
)


class PowerState(Base):
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	prosumer_state = db.Column(db.String(50), nullable=True)
	measured_power = db.Column(db.String(50), nullable=True)
	measured_energy = db.Column(db.String(50), nullable=True)
	energy_today = db.Column(db.String(50), nullable=True)
	time_today = db.Column(db.String(50), nullable=True)
	last_message = db.Column(ArrowType, nullable=True)
	from_buffer = db.Column(db.Boolean, default=False, nullable=True)

	prosumer_id = db.Column(db.String(100), db.ForeignKey('prosumer.prosumer_id', onupdate="CASCADE", ondelete="CASCADE"))

	def set_probe_state(self, probe_id, probe_state, last_message):
		probe = Probe.get_or_create(probe_id)
		probe.state = probe_state
		probe.last_message = last_message

	# def last_state(self):
	# 	return self.
	def to_dict(self):
		response = {
			'probes': [probe.small_dict() for probe in self.prosumer.probes.all()],
			'prosumer_state': self.prosumer_state,
			'measured_power': self.measured_power,
			'measured_energy': self.measured_energy,
			'energy_today': self.energy_today,
			'time_today': self.time_today,
			'last_message': str(self.last_message.to('Africa/Lagos')) if self.last_message else '',
			'date_modified': str(self.date_modified) if self.date_modified else '',
			'from_buffer': int(str(self.from_buffer) == str(True)),
		}
		return response


class Probe(Base):
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	probe_id = db.Column(db.String(100), unique=True, nullable=False)
	state = db.Column(db.String(100), nullable=True)
	last_message = db.Column(ArrowType, nullable=True)
	prosumers = db.relationship('Prosumer', secondary='probe_prosumers',
		backref=db.backref('probes', lazy='dynamic'))

	def __init__(self, probe_id):
		self.probe_id = probe_id

	@staticmethod
	def get_or_create(probe_id):
		probe = Probe.query.filter_by(probe_id=probe_id).first()
		if not probe:
			probe = Probe(probe_id=probe_id)
			db.session.add(probe)
		return probe

	def record_down_state(self, state, last_message):
		self.state = state
		self.last_message = last_message
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self

	def get_data(self):
		return self.to_dict()

	def small_dict(self):
		response = {
			'probe_id': self.probe_id,
			'state': self.state,
			'last_message': str(self.last_message) if self.last_message else self.last_message,
		}
		return response

	def to_dict(self):
		response = {
			'probe_id': self.probe_id,
			'state': self.state,
			'prosumers': [prosumer.to_dict() for prosumer in self.prosumers]
		}
		return response


class Admin(Base):
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	admin_id = db.Column(db.String(100), unique=True, nullable=False)
	prosumers = db.relationship('Prosumer', secondary='prosumer_admins',
		backref=db.backref('admins', lazy='dynamic'))

	def __init__(self, admin_id):
		self.admin_id = admin_id

	@staticmethod
	def get_or_create(admin_id):
		admin = Admin.query.filter_by(admin_id=admin_id).first()
		if not admin:
			admin = Admin(admin_id=admin_id)
			db.session.add(admin)
		return admin

	@staticmethod
	def get_one(admin_id):
		return Admin.query.filter_by(admin_id=admin_id).first()

	@staticmethod
	def get_probes(admin_id):
		admin = Admin.get_one(admin_id)
		probes = list(set(chain([prosumer.associated_probes() for prosumer in admin.prosumers])))
		probes = [probe.to_dict() for probe in probes]
		return probes

	@staticmethod
	def get_prosumers(admin_id):
		admin = Admin.get_one(admin_id)
		probes = [prosumer.to_dict() for prosumer in admin.prosumers]
		return probes


class Prosumer(Base):
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)

	prosumer_id = db.Column(db.String(100), unique=True, nullable=False)
	prosumer_name = db.Column(db.String(100), nullable=True)

	user_group_id = db.Column(db.String(100), nullable=True)
	user_full_name = db.Column(db.String(100), nullable=True)

	configuration_id = db.Column(db.String(100), nullable=True)

	facility_id = db.Column(db.String(100), nullable=True)
	facility_name = db.Column(db.String(100), nullable=True)

	prosumer_url = db.Column(db.String(100), nullable=True)

	ignored = db.Column(db.Boolean, default=False)

	power_states = db.relationship('PowerState', backref='prosumer', lazy='dynamic', cascade="all, delete-orphan")

	@staticmethod
	def create(prosumer_id, prosumer_name=None, probe_names=[], user_group_id=None, configuration_id=None, facility_id=None, prosumer_url=None,
		client_admins=[], user_full_name=None, facility_name=None, prosumer_state=None):
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if not prosumer:
			prosumer = Prosumer(
				prosumer_id=prosumer_id,
				prosumer_name=prosumer_name,
				user_group_id=user_group_id,
				user_full_name=user_full_name,
				configuration_id=configuration_id,
				facility_id=facility_id,
				facility_name=facility_name,
				prosumer_url=prosumer_url,
			)
		else:
			prosumer.prosumer_name = prosumer_name
			prosumer.user_group_id = user_group_id
			prosumer.user_full_name = user_full_name
			prosumer.configuration_id = configuration_id
			prosumer.facility_id = facility_id
			prosumer.facility_name = facility_name
			prosumer.prosumer_url = prosumer_url
		db.session.add(prosumer)
		if prosumer_state:
			state = PowerState(prosumer_state=prosumer_state)
			prosumer.power_states.append(state)
		for probe_id in probe_names:
			probe = Probe.get_or_create(probe_id)
			if prosumer not in probe.prosumers:
				probe.prosumers.append(prosumer)
		if user_group_id:
			client_admins.append(user_group_id)

		for admin_id in client_admins:
			admin = Admin.get_or_create(admin_id)
			if prosumer not in admin.prosumers:
				admin.prosumers.append(prosumer)

		db.session.add(prosumer)
		if user_full_name:
			prosumer.user_full_name = user_full_name
		if facility_name:
			prosumer.facility_name = facility_name

		db.session.add(prosumer)
		db.session.commit()
		db.session.refresh(prosumer)
		# prosumer = add_and_commit(prosumer)
		return prosumer

	def edit(self, prosumer_name=None, probe_names=[], user_group_id=None, configuration_id=None, facility_id=None, prosumer_url=None,
		client_admins=[], user_full_name=None, facility_name=None, prosumer_state=None):
		self.prosumer_name = prosumer_name
		self.user_group_id = user_group_id
		self.user_full_name = user_full_name
		self.configuration_id = configuration_id
		self.facility_id = facility_id
		self.facility_name = facility_name
		self.prosumer_url = prosumer_url

		for probe_id in probe_names:
			probe = Probe.get_or_create(probe_id)
			if self not in probe.prosumers:
				probe.prosumers.append(self)
		if user_group_id:
			client_admins.append(user_group_id)

		if prosumer_state:
			state = PowerState(prosumer_state=prosumer_state)
			self.power_states.append(state)

		added_admins = set(client_admins)
		existing_admins = set([admin.admin_id for admin in self.admins.all()])
		new_admins = list(added_admins - existing_admins)
		deleted_admins = list(existing_admins - added_admins)

		for admin_id in new_admins:
			admin = Admin.get_or_create(admin_id)
			if self not in admin.prosumers:
				admin.prosumers.append(self)

		for admin_id in deleted_admins:
			admin = Admin.get(admin_id)
			if self in admin.prosumers:
				admin.prosumers.remove(self)

		db.session.add(self)
		if user_full_name:
			self.user_full_name = user_full_name
		if facility_name:
			self.facility_name = facility_name

		prosumer = add_and_commit(self)
		return prosumer

	@staticmethod
	def get_one(prosumer_id=None, name=None, ignored=False):
		if prosumer_id:
			return Prosumer.query.filter_by(prosumer_id=prosumer_id)\
				.filter_by(ignored=ignored).first()
		elif name:
			return Prosumer.query.filter_by(prosumer_name=name)\
				.filter_by(ignored=ignored).first()
		return None

	@staticmethod
	def get_one_or_create(prosumer_id, configuration_id, probe_names):
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if not prosumer:
			prosumer = Prosumer.create(prosumer_id, configuration_id=configuration_id, probe_names=probe_names)
		return prosumer

	@staticmethod
	def by_user_group(user_group_id):
		return [prosumer.latest() for prosumer in Prosumer.query.filter_by(user_group_id=user_group_id).all()]

	@staticmethod
	def by_configs(configuration_ids):
		return [prosumer.latest() for prosumer in Prosumer.query.filter(Prosumer.configuration_id.in_(configuration_ids)).all()]

	@staticmethod
	def set_user_name(prosumer_name, user_full_name):
		prosumer = Prosumer.get_one(name=prosumer_name)
		prosumer.user_full_name = user_full_name
		prosumer = add_and_commit(prosumer)
		return prosumer

	@staticmethod
	def set_user_group_id(prosumer_name, user_group_id):
		prosumer = Prosumer.get_one(name=prosumer_name)
		prosumer.user_group_id = user_group_id
		prosumer = add_and_commit(prosumer)
		return prosumer

	def set_user(self, user_group_id, user_full_name):
		self.user_full_name = user_full_name
		self.user_group_id = user_group_id
		prosumer = add_and_commit(self)
		return prosumer

	def latest_state(self):
		last_state = self.power_states.order_by(PowerState.date_modified.desc()).first()
		return last_state.to_dict() if last_state else {}

	def record_power_state(self, probe_id, probe_state, prosumer_state, prosumer_name=None, measured_power=None, 
		measured_energy=None, energy_today=None, time_today=None, last_message=None, from_buffer=None):
		self.prosumer_name = prosumer_name
		last_message = arrow.get(last_message).to('Africa/Lagos')
		new_state = PowerState(
			prosumer_state=prosumer_state,
			measured_power=measured_power,
			measured_energy=measured_energy,
			energy_today=energy_today,
			time_today=time_today,
			last_message=last_message
		)
		if from_buffer:
			new_state.from_buffer = bool(from_buffer)
		new_state.set_probe_state(probe_id, probe_state, last_message)
		self.power_states.append(new_state)
		# prosumer = add_and_commit(self)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self

	def record_down_state(self, prosumer_state, user_full_name=None, user_group_id=None, last_message=None):
		new_state = PowerState(
			prosumer_state=prosumer_state,
			last_message=arrow.get(last_message).to('Africa/Lagos')
		)
		if user_full_name:
			self.user_full_name = user_full_name
		if user_group_id:
			self.user_group_id = user_group_id
		self.power_states.append(new_state)
		# prosumer = add_and_commit(self)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self

	@staticmethod
	def set_facility_id(prosumer_name, facility_id):
		prosumer = Prosumer.get_one(name=prosumer_name)
		prosumer.facility_id = facility_id
		prosumer = add_and_commit(prosumer)
		return prosumer

	@staticmethod
	def set_facility_name(prosumer_name, facility_name):
		prosumer = Prosumer.get_one(name=prosumer_name)
		prosumer.facility_name = facility_name
		prosumer = add_and_commit(prosumer)
		return prosumer

	@staticmethod
	def set_prosumer_id(prosumer_name, prosumer_id):
		prosumer = Prosumer.get_one(name=prosumer_name)
		prosumer.prosumer_id = prosumer_id
		prosumer = add_and_commit(prosumer)
		return prosumer

	@staticmethod
	def set_prosumer_url(prosumer_name, prosumer_url):
		prosumer = Prosumer.get_one(name=prosumer_name)
		prosumer.prosumer_url = prosumer_url
		prosumer = add_and_commit(prosumer)
		return prosumer

	# def update_prosumer(self, prosumer_state, last_message):
	# 	self.state = prosumer_state
	# 	self.last_message = arrow.get(last_message).to('Africa/Lagos')
	# 	add(self)
	# 	return self

	def ignore(self):
		self.ignored = True
		prosumer = add_and_commit(self)
		return True if prosumer else False

	@staticmethod
	def delete_one(prosumer_id):
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if prosumer:
			db.session.delete(prosumer)
			commit()
			return True
		return False

	def delete(self):
		db.session.delete(self)
		db.session.commit()
		return True

	@property
	def associated_probes(self):
		return self.probes.all()

	@property
	def associated_admins(self):
		return self.admins.all()

	@staticmethod
	def all_prosumers():
		# todo: paginate this.
		prosumers = Prosumer.query.order_by(Prosumer.user_full_name.asc()).all()
		logger.info('\n\n\n')
		logger.info([prosumer.latest() for prosumer in prosumers])
		logger.info('\n\n\n')
		return [prosumer.latest() for prosumer in prosumers]

	def get_data(self):
		return self.to_dict()

	def to_dict(self):
		response = {
			'prosumer_id': self.prosumer_id,
			'prosumer_name': self.prosumer_name,
			'user_group_id': self.user_group_id,
			'user_full_name': self.user_full_name,
			'configuration_id': self.configuration_id,
			'facility_id': self.facility_id,
			'facility_name': self.facility_name,
			'prosumer_url': self.prosumer_url,
			'state_info': [state.to_dict() for state in self.power_states.all()],
			# 'probes': [probe.small_dict() for probe in self.associated_probes],
		}
		return response

	@staticmethod
	def commit():
		try:
			db.session.commit()
		except:
			db.session.rollback()

	def latest(self):
		latest_state = self.latest_state()
		response = {
			'prosumer_id': self.prosumer_id,
			'prosumer_name': self.prosumer_name,
			'user_group_id': self.user_group_id,
			'user_full_name': self.user_full_name,
			'configuration_id': self.configuration_id,
			'facility_id': self.facility_id,
			'facility_name': self.facility_name,
			'prosumer_url': self.prosumer_url,
			**latest_state
			# 'prosumer_state': latest_state['prosumer_state'] if 'prosumer_state' in latest_state else None,
			# 'state_info': latest_state,
			# 'probes': [probe.small_dict() for probe in self.associated_probes],
		}
		return response
