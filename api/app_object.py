from api import create_api
from os import environ


if environ.get('FLASK_ENV') is None:
    print('FLASK_ENV not set')
mode = environ.get('FLASK_ENV') or 'default'
app = create_api(mode, main=True)
