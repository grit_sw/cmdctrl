import click
from flask import Flask
import subprocess
import sys
from pathlib import Path
from dotenv import load_dotenv
import multiprocessing


env_path = str(Path('.') / '.flaskenv')
load_dotenv(dotenv_path=env_path)


app = Flask(__name__)


@app.cli.command()
def online():
	"""Monitor prosumer states"""
	# online = 'gunicorn -b :5200 -k eventlet -t 0 run_online:app'.split(' ')
	# online = 'gunicorn -b :5200 -k eventlet -t 2000 run_online:app'.split(' ')
	# online = 'gunicorn -k eventlet -t 2000 run_online:app'.split(' ')
	online = 'python run_online.py'.split(' ')
	ret = subprocess.call(online)
	sys.exit(ret)


@app.cli.command()
def offline():
	"""Monitor prosumers for offline states"""
	offline = 'python run_offline.py'.split(' ')
	# offline = 'gunicorn -b :5300 -k eventlet -t 2000 run_offline:app'.split(' ')
	ret = subprocess.call(offline)
	sys.exit(ret)


@app.cli.command()
def api():
	"""CMDCTRL api server."""
	ret = subprocess.call(
			# ['gunicorn', '-k', 'eventlet', '-b', ':5200', 'run_api:app']
			'gunicorn -b :5000 -k eventlet --access-logfile - --error-logfile - run_api:app'.split(' ')
		)
	sys.exit(ret)
