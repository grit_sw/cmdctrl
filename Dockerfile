FROM alpine:3.7 as base

WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3 postgresql-libs librdkafka-dev
RUN apk add --virtual .build-deps \
    gcc \
    musl-dev \
    postgresql-dev \
    python3-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

COPY api api
COPY stream stream
COPY config.py .flaskenv celeryconfig.py cli_tools.py logger.py ./

ENV FLASK_ENV Staging
EXPOSE 5000
# ENV FLASK_APP run_api.py


FROM base as api
WORKDIR /home/user/
COPY --from=base /home/user/api .
COPY --from=base /home/user/cli_tools.py .
COPY run_api.py start_api.sh ./
RUN chmod +x start_api.sh
ENTRYPOINT ["./start_api.sh"]


FROM base as online
COPY --from=base /home/user/api .
COPY --from=base /home/user/stream .
COPY --from=base /home/user/cli_tools.py .
COPY stream-new-v0.avsc run_online.py start_online.sh ./
RUN chmod +x start_online.sh
ENTRYPOINT ["./start_online.sh"]

FROM base as offline
COPY --from=base /home/user/api .
COPY --from=base /home/user/stream .
COPY --from=base /home/user/cli_tools.py .
COPY run_offline.py start_offline.sh ./
RUN chmod +x start_offline.sh
ENTRYPOINT ["./start_offline.sh"]
