#!/usr/bin/env python3
from stream import OfflineChecker
from api.celery_aux import app

checker = OfflineChecker()
with app.app_context():
	checker.run()
