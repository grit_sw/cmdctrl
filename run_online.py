#!/usr/bin/env python3
from stream import ProsumerMonitor
from api.celery_aux import app
import os
from ast import literal_eval


from eventlet import monkey_patch
monkey_patch() # Here so that socketio will work
print(os.getenv('MQTT_SERVER_URL'))
monitor = ProsumerMonitor(
	bootstrap_servers=os.getenv('BOOTSTRAP_SERVERS'),
	schema_registry=os.getenv('SCHEMA_REGISTRY_URI'),
	group_id=os.getenv('GROUP_ID'),
	topics=literal_eval(os.getenv('SUBSCRIBE_TO')),
	mqtt_server_url=os.getenv('MQTT_SERVER_URL'),
	mqtt_server_port=os.getenv('MQTT_SERVER_PORT'),
	# mqtt_cert_name=os.getenv('MQTT_CERT_NAME'),
)


with app.app_context():
	monitor.run()
