CMDCTRL_DATABASE_URL='postgresql://postgres:postgres@localhost/cmdctrl_db'
STREAM_DATABASE_URL='postgresql://postgres:postgres@localhost/stream_db'
MONGO_DATABASE_URL='mongodb://localhost:27017'
SECRET_KEY=b'3420662f27a047b023945c7430553a7c3d476208d618bd8b'
FLASK_APP=cli_tools.py
OFFLINE_SLEEP_TIME=5

SOCKETIO_MESSAGE_QUEUE='redis://'
PROSUMER_DESTROY_URI='http://localhost:5900/prosumers/destroy-prosumer/{}/{}/'
GROUP_USER_URI='http://localhost:5900/users/max-admin/{group_id}/'
FACILITY_NAME_URI='http://localhost:5900/facility/{facility_id}/'
ONE_PROSUMER_URI='http://localhost:5900/prosumers/one-prosumer/{prosumer_name}/'

ONE_ENERGY_PLAN_URI=http://localhost:5900/energy_plans/tx-engine/{plan_id}/
ONE_ACCOUNT_URI=http://localhost:5900/account/tx-engine/{group_id}/
PROSUMER_BUILDER_URI=http://localhost:5900/prosumers/build-prosumer-configuration/{prosumer_id}/
UNASSIGN_ALL_PROSUMERS_FROM_USERS=http://localhost:5900/prosumers/unassign-all/{user_group_id}/
GET_ONE_PROSUMER_URI=http://localhost:5900/prosumers/get-one/{prosumer_id}/

MAX_ADMIN_IN_GROUP_URI=http://localhost:5900/users/max-admin/
GET_ONE_CONFIGURATION_URI=http://localhost:5900/configuration/one/

BOOTSTRAP_SERVERS='dashboard.grit.systems'
SCHEMA_REGISTRY_URI='http://dashboard.grit.systems:8081'
GROUP_ID='dev_prosumer_monitor'
SUBSCRIBE_TO=['prosumer-update', 'gritServer', 'prepare-config', 'probe-stream', 'user-update']

MQTT_SERVER_URL="mqtt.grit.systems"
MQTT_SERVER_PORT=8883
MQTT_CERT_NAME=cacert.pem

CMDCTRL_IGNORE_PROSUMER_URL='http://localhost:5000/cmdctrl/ignore-prosumer/{}/'
ALL_CMDCTRL_PROSUMERS='http://localhost:5000/cmdctrl/all-prosumers/'
CMDCTRL_NEW_PROSUMER_URL='http://localhost:5000/cmdctrl/new/'
CMDCTRL_UPDATE_PROSUMER_URL='http://localhost:5000/cmdctrl/edit/'
CMDCTRL_UPDATE_USER_URL='http://localhost:5000/cmdctrl/update-user/'
CMDCTRL_RECORD_UP_PROSUMER_URL='http://localhost:5000/cmdctrl/record-power-state/'
CMDCTRL_RECORD_DOWN_PROSUMER_URL='http://localhost:5000/cmdctrl/record-down-state/'
CMDCTRL_RECORD_DOWN_PROBE_URL='http://localhost:5000/cmdctrl/record-probe-down-state/'
