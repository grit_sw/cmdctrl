import requests
from logger import logger
from os import getenv
import logging


def build_account_prosumer_dict(prosumers):
	response_dict = {}
	for prosumer in prosumers['data']:
		prosumer_id = prosumer['prosId']
		user_group_id = prosumer['ugId']
		response_dict[prosumer_id] = user_group_id
	return response_dict


def build_energy_plan_prosumer_dict(prosumers):
	plan_prosumer_dict = {}
	for prosumer in prosumers['data']:
		prosumer_id = prosumer['prosId']
		plan_id = prosumer['planId']
		plan_prosumer_dict[prosumer_id] = plan_id
	return plan_prosumer_dict


class CreateConfig(object):
	def __init__(self, prosumer_id):
		self.prosumer_id = prosumer_id
		self.prosumer_api = getenv('PROSUMER_BUILDER_URI')
		self.energyplan_api = getenv('ONE_ENERGY_PLAN_URI')
		self.account_api = getenv('ONE_ACCOUNT_URI')

	def query_prosumer(self):
		prosumer = requests.get(self.prosumer_api.format(prosumer_id=self.prosumer_id))
		if prosumer.status_code == 200:
			prosumers = prosumer.json()['data']
			return prosumers
		return []

	def query_energy_plans(self, plan_id):
		energyplan = requests.get(self.energyplan_api.format(plan_id=plan_id))
		if energyplan.status_code == 200:
			energyplan = energyplan.json()['data']
			return energyplan
		return []
	
	def query_accounts(self, user_group_id):
		account = requests.get(self.account_api.format(group_id=user_group_id))
		if account.status_code == 200:
			account = account.json()['data']
			return account
		return []
	
	def build_params(self):
		prosumers = self.query_prosumer()
		print("\n\n PROSUMERS = ", prosumers['data'])
		if prosumers:
			prosumer_energyplan_builder = build_energy_plan_prosumer_dict(prosumers)
			unique_energyplan_dict = {}
			prosumer_energyplan_dict = {}
			for prosumer_id, plan_id in prosumer_energyplan_builder.items():
				plan = unique_energyplan_dict.get(plan_id, None)
				if not isinstance(plan, dict):
					plan = self.query_energy_plans(plan_id)
					if plan:
						unique_energyplan_dict[plan_id] = plan
				prosumer_energyplan_dict[prosumer_id] = plan
			prosumer_account_builder = build_account_prosumer_dict(prosumers)
			unique_accounts_dict = {}
			prosumer_account_dict = {}
			for prosumer_id, user_group_id in prosumer_account_builder.items():
				account = unique_accounts_dict.get(user_group_id, None)
				if not isinstance(account, dict):
					account = self.query_accounts(user_group_id)
					if account:
						unique_accounts_dict[user_group_id] = account
				prosumer_account_dict[prosumer_id] = account
			prosumer_config = {}
			configs = []
			for prosumer in prosumers['data']:
				prosumer_id = prosumer['prosId']
				energyplan = prosumer_energyplan_dict.get(prosumer_id)
				account = prosumer_account_dict.get(prosumer_id)
				config = {
					'sN': prosumer['sN'],
					'prosId': prosumer_id,
					'fId': prosumer['fId'],
					'ptId': prosumer['ptId'],
					'pgId': prosumer['pgId'],
					'rate': energyplan.get('rate'),
					'enLim': energyplan.get('energy_limit'),
					'powLim': energyplan.get('power_limit'),
					'actBal': account.get('available_balance'),
				}
				configs.append(config)

			prosumer_config['mid'] = prosumers['mid']
			prosumer_config['data'] = configs

			logger.info("\n\n\n")
			logger.info("Prosumer Config = {}".format(prosumer_config))
			return prosumer_config
