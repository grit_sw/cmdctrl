import os


class BuyOrderMatch(object):
	def __init__(self, message):
		self.buy_order_matches = os.getenv("FIND_BUYORDER_MATCH")
		self.message = message

	def calculate_max_price(self):
		rate = self.message['rate']
		start_time = self.message['start_time']
		end_time = self.message['end_time']
		if end_time > start_time:
			raise ValueError("The end time of the order should be greater than the start time.")
		order_duration = end_time - start_time
		
