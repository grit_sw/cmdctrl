from os import getenv
import requests


def unassign_all(user_group_id):
	cookies = {'role_id': '6'}

	response = requests.post(getenv('UNASSIGN_ALL_PROSUMERS_FROM_USERS').format(user_group_id=user_group_id), cookies=cookies)
	if response.status_code == 200:
		response_ = response.json()
		return response_
	return None
