import os

import arrow
import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, backref, relationship
from sqlalchemy_utils import ArrowType

DeclarativeBase = declarative_base()
db_url = os.getenv('CMDCTRL_DATABASE_URL')
engine = db.create_engine(db_url)
session = Session(engine)


def commit():
	session.commit()


class Base(DeclarativeBase):
	__abstract__ = True
	id_ = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(ArrowType, default=db.func.current_timestamp())
	date_modified = db.Column(ArrowType, default=db.func.current_timestamp(),
								onupdate=db.func.current_timestamp())


prosumer_admins = db.Table('prosumer_admins', DeclarativeBase.metadata, 
	db.Column('admin_id', db.String(100), db.ForeignKey('admin.admin_id')),
	db.Column('prosumer_id', db.String(100), db.ForeignKey('prosumer.prosumer_id')),
)


probe_prosumers = db.Table('probe_prosumers', DeclarativeBase.metadata, 
	db.Column('probe_id', db.String(100), db.ForeignKey('probe.probe_id')),
	db.Column('prosumer_id', db.String(100), db.ForeignKey('prosumer.prosumer_id')),
)


prosumer_configs = db.Table('prosumer_configs', DeclarativeBase.metadata,
	db.Column('configuration_id', db.String(100), db.ForeignKey('configuration.configuration_id')),
	db.Column('prosumer_id', db.String(100), db.ForeignKey('prosumer.prosumer_id')),
)


probe_configs = db.Table('probe_configs', DeclarativeBase.metadata,
	db.Column('configuration_id', db.String(100), db.ForeignKey('configuration.configuration_id')),
	db.Column('probe_id', db.String(100), db.ForeignKey('probe.probe_id')),
)


class Admin(Base):
	__tablename__ = 'admin'
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	admin_id = db.Column(db.String(100), unique=True, nullable=False)
	prosumers = relationship('Prosumer', secondary='prosumer_admins',
		backref=backref('admins', lazy='dynamic'))

	@staticmethod
	def get_or_create(admin_id):
		admin = session.query(Admin).filter_by(admin_id=admin_id).first()
		if not admin:
			admin = Admin(admin_id=admin_id)
			session.add(admin)
		return admin

class Configuration(Base):
	__tablename__ = 'configuration'
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	configuration_id = db.Column(db.String(100), unique=True, nullable=True)
	prosumers = relationship('Prosumer', secondary='prosumer_configs',
		backref=backref('configurations', lazy='dynamic'))
	probes = relationship('Probe', secondary='probe_configs',
		backref=backref('configurations', lazy='dynamic'))


	@staticmethod
	def get_or_create(configuration_id):
		configuration = session.query(Configuration).filter_by(configuration_id=configuration_id).first()
		if not configuration:
			configuration = Configuration(configuration_id=configuration_id)
			session.add(configuration)
		return configuration

class Probe(Base):
	__tablename__ = 'probe'
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	probe_id = db.Column(db.String(100), unique=True, nullable=False)
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))
	prosumers = relationship('Prosumer', secondary='probe_prosumers',
		backref=backref('probes', lazy='dynamic'))

	@staticmethod
	def get_or_create(probe_id, config_ids):
		probe = session.query(Probe).filter_by(probe_id=probe_id).first()
		if not probe:
			probe = Probe(probe_id=probe_id)
			for config_id in config_ids:
				configuration = Configuration.get_or_create(config_id)
				if probe not in configuration.probes:
					configuration.probes.append(probe)
				session.add(probe)
		return probe

	@staticmethod
	def create(probe_id, configuration_id):
		"""Called if only a probe is to be registered"""
		probe = Probe.get_or_create(probe_id, configuration_id)
		session.commit()
		return probe

	def room_info(self):
		response = {
			'configurations': [configuration.configuration_id for configuration in self.configurations.all()],
		}
		return response


class Prosumer(Base):
	__tablename__ = 'prosumer'
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	prosumer_id = db.Column(db.String(100), unique=True, nullable=False)
	facility_id = db.Column(db.String(100), unique=False, nullable=True)
	state = db.Column(db.String)
	state_changed = db.Column(db.Boolean)
	last_emit_time = db.Column(ArrowType)
	configuration_id = db.Column(db.String(100), db.ForeignKey('configuration.configuration_id'))

	@staticmethod
	def new(prosumer_id, facility_id=None, configurations=None, user_group_id=None, probe_ids=None, admin_ids=None):
		prosumer = session.query(Prosumer).filter_by(prosumer_id=prosumer_id).first()
		if not prosumer:
			prosumer = Prosumer(
				prosumer_id=prosumer_id,
			)
		if facility_id:
			prosumer.facility_id = facility_id
		if isinstance(configurations, list):
			for config_id in configurations:
				configuration = Configuration.get_or_create(config_id)
				if prosumer not in configuration.prosumers:
					configuration.prosumers.append(prosumer)

		if user_group_id:
			if isinstance(admin_ids, list):
				admin_ids.append(user_group_id)
			else:
				admin_ids = [user_group_id]

		if isinstance(probe_ids, list):
			for probe_id in probe_ids:
				probe = Probe.get_or_create(probe_id, configurations)
				if prosumer not in probe.prosumers:
					probe.prosumers.append(prosumer)

		if isinstance(admin_ids, list):
			prosumer.add_admins(admin_ids)

		session.add(prosumer)
		session.commit()
		session.refresh(prosumer)
		return prosumer

	def emit(self):
		self.last_emit_time = arrow.now('Africa/Lagos')
		session.add(self)
		# session.commit()
		# session.refresh(self)
		return self

	def is_same_state(self, state):
		return self.state == state

	def reset_state(self):
		self.state_changed = False
		session.add(self)
		# session.commit()
		# session.refresh(self)
		return self

	def change_state(self, state):
		self.state = state
		self.state_changed = True
		session.add(self)
		# session.commit()
		# session.refresh(self)
		return self

	@staticmethod
	def get_one(prosumer_id):
		return session.query(Prosumer).filter_by(prosumer_id=prosumer_id).first()

	@staticmethod
	def get_user(prosumer_id, user_group_id):
		return session.query(Prosumer).join(Prosumer.admins)\
			.filter_by(prosumer_id=prosumer_id)\
			.filter_by(admin_id=user_group_id)\
			.first()

	def add_admins(self, admin_ids):
		for admin_id in admin_ids:
			admin = Admin.get_or_create(admin_id)
			if self not in admin.prosumers:
				admin.prosumers.append(self)

	def room_info(self):
		response = {
			'prosumer_id': self.prosumer_id,
			'facility_id': self.facility_id,
			'configurations': [configuration.configuration_id for configuration in self.configurations.all()],
			'admins': [admin.admin_id for admin in self.admins.all()],
		}
		return response

	def to_dict(self):
		response = {
			'prosumer_id': self.prosumer_id,
			'state': self.state,
			'state_changed': self.state_changed,
			'last_emit_time': self.last_emit_time,
			'facility_id': self.facility_id,
			'configurations': [configuration.configuration_id for configuration in self.configurations.all()],
			'admins': [admin.admin_id for admin in self.admins.all()],
		}
		return response


# DeclarativeBase.metadata.drop_all(engine)
DeclarativeBase.metadata.create_all(engine)
