import requests
from os import getenv
from logger import logger


class ApiQuery(object):
	def __init__(self):
		self.users_api = getenv('GROUP_USER_URI')
		self.prosumer_api = getenv('ONE_PROSUMER_URI')
		self.facility_api = getenv('FACILITY_NAME_URI')

	def get_client_admins(self, prosumer_name):
		client_admins = None
		log_update = False
		prosumer = requests.get(self.prosumer_api.format(prosumer_name=prosumer_name))
		if prosumer.status_code == 200:
			prosumer_data = prosumer.json()['data']
			client_admins = prosumer_data.get('client_admins')
		if client_admins:
			log_update = True
		return log_update, client_admins

	def get(self, prosumer_name):
		group_id = None
		facility_id = None
		user_full_name = None
		facility_name = None
		client_admins = None
		try:
			if prosumer_name:
				prosumer = requests.get(self.prosumer_api.format(prosumer_name=prosumer_name))
				if prosumer.status_code == 200:
					prosumer_data = prosumer.json()['data']
					group_id = prosumer_data.get('user_group_id')
					facility_id = prosumer_data.get('facility_id')
					client_admins = prosumer_data.get('client_admins')
		except Exception as e:
			logger.exception(e)
			pass

		try:
			user_full_name = 'UNASSIGNED'
			if group_id:
				user = requests.get(self.users_api.format(group_id=group_id))
				if user.status_code == 200:
					user_data = user.json()['data']
					first_name = user_data.get('first_name')
					last_name = user_data.get('last_name')
					user_name = first_name and last_name
					if user_name:
						user_full_name = '{} {}'.format(first_name, last_name)
		except Exception as e:
			logger.exception(e)
			pass

		try:
			if facility_id:
				facility = requests.get(self.facility_api.format(facility_id=facility_id))
				if facility.status_code == 200:
					facility_data = facility.json()['data']
					facility_name = facility_data.get('facility_name')
		except Exception as e:
			logger.exception(e)
			pass

		prosumer_update = {
			'prosumer_name': prosumer_name,
			'prosumer_user': user_full_name,
			'user_group_id': group_id,
			'facility_name': facility_name,
			'facility_id': facility_id,
			'client_admins': client_admins,
		}
		log_update = True
		if not any((user_full_name, group_id, facility_name, facility_id, client_admins)):
			log_update = False
		return log_update, prosumer_update