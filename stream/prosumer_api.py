"""
	Module for interacting with the prosumer api.
	Actions:
	- get a prosumer via the prosumer's group ID
"""
from flask import current_app
import requests


class ProsumerApi:
	def __init__(self):
		self.cookies = {'role_id': '6'}
		self.one_prosumer_uri = current_app.config.get('GET_ONE_PROSUMER_URI')

	def get_prosumer(self, prosumer_id):
		response = requests.get(
			self.one_prosumer_uri.format(prosumer_id=prosumer_id),
			cookies=self.cookies,
		)
		if response.status_code != 200:
			return None

		return response.json()['data']

	def get_prosumer_full_name(self, prosumer_id):
		prosumer = self.get_prosumer(prosumer_id)
		if not prosumer:
			return 'UNASSIGNED'
		return  f"{prosumer['first_name']} {prosumer['last_name']}"
