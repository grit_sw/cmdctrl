"""
	Module for interacting with the user api.
	Actions:
	- get a user via the user's group ID
"""
from os import getenv

import requests
from logger import logger


class UserApi:
	def __init__(self):
		self.cookies = {'role_id': '6'}
		self.user_group_uri = getenv('MAX_ADMIN_IN_GROUP_URI')
		self.configuration_uri = getenv('GET_ONE_CONFIGURATION_URI')

	def get_user(self, user_group_id):
		url = f"{self.user_group_uri}{user_group_id}/"
		logger.info(url)
		try:
			response = requests.get(
				url,
				cookies=self.cookies,
			)
			if not response.ok:
				logger.info(response.status_code)
				logger.info(response.json())
				return None
		except requests.exceptions.ConnectionError as e:
			if getenv('FLASK_ENV') in {'production', 'Production'}:
				logger.critical(e)
			return None

		return response.json()['data']

	def get_config_user(self, configuration_id):
		url = f"{self.configuration_uri}{configuration_id}/"
		logger.info(url)
		try:
			response = requests.get(
				url,
				cookies=self.cookies,
			)
			if not response.ok:
				logger.info(response.status_code)
				logger.info(response.json())
				return None
		except requests.exceptions.ConnectionError as e:
			if getenv('FLASK_ENV') in {'production', 'Production'}:
				logger.critical(e)
			return None
		return response.json()['data']

	def get_user_full_name(self, user_group_id=None, configuration_id=None):
		if user_group_id:
			user = self.get_user(user_group_id)
			user_full_name = f"{user['first_name']} {user['last_name']}"
			if not user:
				user = self.get_config_user(configuration_id)
				if not user:
					return None, user_group_id
				user_full_name = f"{user['admin_name']}-{user['configuration_name']}"
				user_group_id = user['admin_id']
			return  user_full_name, user_group_id
		if configuration_id:
			user = self.get_config_user(configuration_id)
			if not user:
				return None, user_group_id
			user_full_name = f"{user['admin_name']}-{user['configuration_name']}"
			user_group_id = user['admin_id']
		return  user_full_name, user_group_id
