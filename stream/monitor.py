import os
from queue import Queue, Empty
from threading import Thread
from time import sleep, time

import arrow
import requests
from confluent_kafka import KafkaError, Producer
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError
from flask import current_app

from api.events import push_message
from logger import logger
from stream.api_routes import unassign_all
from stream.models import Prosumer, commit
from stream.meter import ProsumerStream
from stream.user_api import UserApi


api_queue = Queue(maxsize=0)

def add_to_queue(func, arg):
	api_queue.put((func, arg))


def perform_func():
	while True:
		try:
			func, arg = api_queue.get(timeout=.01)
		except Empty:
			# logger.info('Empty queue waiting for messages')
			sleep(0.01)
			continue
		except requests.exceptions.ConnectionError as e:
			logger.critical('Api connection error.{}'.format(e))
			sleep(0.01)
			continue
		if hasattr(func, '__call__'):
			func(arg)
		else:
			sleep(0.01)
			continue


def create_thread():
	NUM_THREADS = 10
	for _ in range(NUM_THREADS):
		thread = Thread(target=perform_func)
		thread.start()


def get_facility_name(facility_id):
	url = os.getenv('FACILITY_NAME_URI')
	if not url:
		return None
	url = url.format(facility_id)
	resp = requests.get(url, cookies={'role_id': '6'})
	if resp.ok:
		return resp.json()['data']['facility_name']
	return None

def record_prosumer_installation(prosumer_url):
	installation_url = os.getenv('PROSUMER_INSTALLATION_URL')
	url = f"{installation_url}{prosumer_url}/"
	resp = requests.post(url, cookies={'role_id': '6'})
	logger.info('\n')
	logger.info(f'Prosumer installation status code {resp.status_code}')
	logger.info('\n')
	return resp.status_code

def record_prosumer_uninstallation(prosumer_url):
	uninstallation_url = os.getenv('PROSUMER_UNINSTALLATION_URL')
	url = f"{uninstallation_url}{prosumer_url}/"
	resp = requests.post(url, cookies={'role_id': '6'})
	logger.info('\n')
	logger.info(f'Prosumer uninstallation status code {resp.status_code}')
	logger.info('\n')
	return resp.ok

def destroy_prosumer(prosumer_id):
	url = os.getenv('PROSUMER_DESTRUCTION_URL').format(prosumer_id=prosumer_id)
	resp = requests.delete(url, cookies={'role_id': '6'})
	logger.info('\n')
	logger.info(f'Prosumer destruction status code {resp.status_code}')
	logger.info('\n')
	return resp.ok

def ignore_prosumer(prosumer_id):
	url = os.getenv('CMDCTRL_IGNORE_PROSUMER_URL').format(prosumer_id=prosumer_id)
	resp = requests.delete(url, cookies={'role_id': '6'})
	logger.info('\n')
	logger.info(f'Prosumer ignored status code {resp.status_code}')
	logger.info('\n')
	return resp.ok

def get_all_prosumers():
	url = os.getenv('ALL_CMDCTRL_PROSUMERS')
	if url:
		url = url + '?stream=1'
		resp = requests.get(url, cookies={'role_id': '6'}, stream=True)
		if resp.ok:
			return resp.json()['data'] or []
	return []

def new_prosumer(data):
	url = os.getenv('CMDCTRL_NEW_PROSUMER_URL')
	resp = requests.post(url, cookies={'role_id': '6'}, data=data)
	logger.info('\n')
	logger.info(f'CMDCTRL new prosumer status code {resp.status_code}')
	logger.info('\n')
	return resp.status_code

def edit_prosumer(data):
	url = os.getenv('CMDCTRL_UPDATE_PROSUMER_URL')
	resp = requests.post(url, cookies={'role_id': '6'}, data=data)
	logger.info('\n')
	logger.info(f'CMDCTRL prosumer edit status code {resp.status_code}')
	logger.info('\n')
	return resp.status_code

def update_user(data):
	url = os.getenv('CMDCTRL_UPDATE_USER_URL')
	resp = requests.post(url, cookies={'role_id': '6'}, data=data)
	logger.info('\n')
	logger.info(f'CMDCTRL user update status code {resp.status_code}')
	logger.info('\n')
	return resp.status_code

def record_online_prosumer(data):
	url = os.getenv('CMDCTRL_RECORD_UP_PROSUMER_URL')
	resp = requests.post(url, cookies={'role_id': '6'}, data=data)
	logger.info('\n')
	logger.info(f'CMDCTRL power state record status code {resp.status_code}')
	logger.info('\n')
	return resp

def record_down_prosumer(data):
	url = os.getenv('CMDCTRL_RECORD_DOWN_PROSUMER_URL')
	resp = requests.post(url, cookies={'role_id': '6'}, data=data)
	logger.info('\n')
	logger.info(f'CMDCTRL down state record status code {resp.status_code}')
	logger.info('\n')
	return resp

def record_down_probe(data):
	url = os.getenv('CMDCTRL_RECORD_DOWN_PROBE_URL')
	resp = requests.post(url, cookies={'role_id': '6'}, data=data)
	logger.info('\n')
	logger.info(f'CMDCTRL down probe state record status code {resp.status_code}')
	logger.info('\n')
	return resp

def prepare_message(message, prosumer=None):
	if not prosumer:
		room_info = {}
	else:
		room_info = prosumer.room_info()
	return message, room_info

def emit_message(event, message, prosumer=None):
	state_message, room_info = prepare_message(message, prosumer)
	push_message(event, state_message, room_info)

def time_difference(measurement_time):
	return (arrow.now('Africa/Lagos') - arrow.get(measurement_time).to('Africa/Lagos')).seconds

def is_old_message(measurement_time):
	if time_difference(measurement_time) > 25:
		return True
	return False

def is_stale(measurement_time):
	if time_difference(measurement_time) > 15:
		return True
	return False

def to_emit(prosumer, state):
	last_emit_time = prosumer.last_emit_time
	will_emit = False
	if not last_emit_time:
		# never been emitted before (maybe a new prosumer).
		will_emit = True
		return will_emit
	elif is_stale(last_emit_time):
		will_emit = True
		return will_emit

	if not prosumer.is_same_state(state):
		will_emit = True
		prosumer.change_state(state)
	else:
		if prosumer.state_changed:
			prosumer.reset_state()
	return will_emit

class OfflineChecker(object):
	def __init__(self):
		self.offline_exempt_states = {
			# 'OFF',
			# 'OFFLINE',
			# 'BUFFERING',
			'PROBE OFF',
			'NEW METER',
			'UNASSIGNED',
			'NEW USER',  # Should a device be seen as offline if has already been assigned to a user but not configured?
			'POWER LIMIT EXCEEDED',
			'ACCOUNT CREDIT EXHAUSTED',
		}

	def probe_is_offline(self, probe_state):
		print('Probe state', probe_state)
		state, last_probe_message, _ = probe_state
		exempt = state in self.offline_exempt_states
		old_probe_message = is_old_message(last_probe_message)
		return not exempt and old_probe_message

	def probe_is_off(self):
		# Haven't figured out how to do this
		pass

	def probe_is_on(self, probe_states):
		if 'PROBE ON' in set([probe[0] for probe in probe_states]):
			return True
		return False

	def offline_job(self):
		logger.info('Running Offline task')
		all_prosumers = get_all_prosumers() or []
		for prosumer in all_prosumers:
			logger.info('Processing Prosumer: {}'.format(prosumer.get('prosumer_name')))
			# logger.info(prosumer)
			if bool(prosumer['from_buffer']):
				last_prosumer_update = arrow.get(prosumer.get('date_modified')).replace(tzinfo='Africa/Lagos')
				if not is_old_message(last_prosumer_update):
					continue
			probes = prosumer.get('probes')
			probe_states = []
			if isinstance(probes, list):
				probe_states = [(state['state'], state['last_message'], state['probe_id']) for state in probes]
				print('\n\n\t\t\t\t', probe_states, '240\n\n\n')
			for probe_state in probe_states:
				is_offline = self.probe_is_offline(probe_state)
				if is_offline:
					event = 'probe-update'
					message = {
						'probe_id': probe_state[2],
						'state': 'OFFLINE',
						'last_message': probe_state[1],
					}
					# record_down_probe(message)
					add_to_queue(record_down_probe, message)
					emit_message(event, message)

			prosumer_state = prosumer.get('prosumer_state')
			prosumer_id = prosumer.get('prosumer_id')
			prosumer_name = prosumer.get('prosumer_name')
			user_full_name = prosumer.get('user_full_name')
			configuration_id = prosumer.get('configuration_id')
			last_prosumer_message = prosumer.get('last_message')
			last_prosumer_update = prosumer.get('date_modified')
			if not any((prosumer_state, prosumer_id, last_prosumer_message)):
				continue
			old_message = is_old_message(last_prosumer_update)
			probe_offline = all([self.probe_is_offline(probe_state) for probe_state in probe_states]) # set offline if all connected to the prosumer are offline.
			probe_on = self.probe_is_on(probe_states)
			probe_ids = [probe['probe_id'] for probe in probes]
			message = {}
			socket_message = {}
			event = ''
			if all((old_message, probe_offline)):
				prosumer_state = 'OFFLINE'
				event = 'prosumer-update'
				socket_message = {
					'prosumer_id': prosumer_id,
					'prosumer_name': prosumer_name,
					'user_full_name': user_full_name,
					'probe_ids': probe_ids,
					'prosumer_state': prosumer_state,
					'last_message': last_prosumer_message,
				}
				message = {
					'prosumer_id': prosumer_id,
					'configuration_id': configuration_id,
					'probe_ids': probe_ids,
					'prosumer_state': prosumer_state,
					'last_message': last_prosumer_message,
				}
			if all((old_message, probe_on)):
				prosumer_state = 'OFF'
				event = 'prosumer-update'
				socket_message = {
					'prosumer_id': prosumer_id,
					'prosumer_name': prosumer_name,
					'user_full_name': user_full_name,
					'probe_ids': probe_ids,
					'prosumer_state': prosumer_state,
					'last_message': last_prosumer_message,
				}
				message = {
					'prosumer_id': prosumer_id,
					'configuration_id': configuration_id,
					'probe_ids': probe_ids,
					'prosumer_state': prosumer_state,
					'last_message': last_prosumer_message,
				}
			prosumer_ = Prosumer.get_one(prosumer_id)
			if not prosumer_:
				prosumer_ = Prosumer.new(prosumer_id, configurations=[configuration_id])
			if to_emit(prosumer_, prosumer_state):
				if not user_full_name:
					user = {}
					user_full_name, user_group_id = UserApi().get_user_full_name(
						configuration_id=configuration_id
						)
					if user_group_id:
						user['user_group_id'] = user_group_id
						prosumer_.add_admins([user_group_id])
					if user_full_name:
						user['user_full_name'] = user_full_name
						socket_message['user_full_name'] = user_full_name
					if user:
						user['prosumer_id'] = prosumer_id
						add_to_queue(update_user, user)
				if message:
					add_to_queue(record_down_prosumer, message)
					emit_message(event, socket_message, prosumer=prosumer_)
			logger.info(f'Prosumer STATE: {prosumer_state}')
			sleep(0.001)
		commit()


	def run(self):
		create_thread()
		while True:
			self.offline_job()
			sleep(float(current_app.config.get('OFFLINE_SLEEP_TIME')))


class ProsumerMonitor(object):
	"""
		The ProsumerMonitor class that Monitors prosumer health and provides notifications
		to the different teams if something goes wrong with one or more measured_power.
	"""

	def __init__(self, bootstrap_servers, schema_registry, group_id, topics,
		mqtt_server_url, mqtt_server_port):
		# mqtt_server_url, mqtt_server_port, mqtt_cert_name):
		"""
			@:param: bootstrap_servers: Kafka Server(s) to connect to
			@:param: schema_registry: Schema registry url
			@:param: group_id: The prosumer's group ID
			@:param: topics: A list of topics to consume from
		"""
		super(ProsumerMonitor, self).__init__()
		logger.info('Starting Probe/Prosumer Monitor')
		self.bootstrap_servers = bootstrap_servers
		self.schema_registry = schema_registry
		self.mqtt_server_url = mqtt_server_url
		self.mqtt_server_port = mqtt_server_port
		# self.mqtt_cert_name = mqtt_cert_name
		self.string_producer = Producer(
			{'bootstrap.servers': self.bootstrap_servers}
		)
		# self.mqtt_client = MqttClient(self.mqtt_server_url, self.mqtt_server_port).get_client
		# self.mqtt_client = MqttClient(self.mqtt_server_url, self.mqtt_server_port, self.mqtt_cert_name).get_client
		self.group_id = group_id
		self.topics = topics
		self.running = True
		self.probes = ProsumerStream(
			schema_registry=self.schema_registry,
			topic='probe-stream',
			schema_subject='gritServer-value',
			schema_file='stream-new-v0.avsc'
		)
		logger.info('Listening to the following servers {}'.format(self.bootstrap_servers))
		logger.info('Consumer started ...')
		self.consumer = self.create_consumer()


	def create_consumer(self):
		"""
			Method that creates a new avro consumer
			@:param: None
			@:returns: None
		"""
		_prosumer = AvroConsumer({
			'bootstrap.servers': self.bootstrap_servers,
			'group.id': self.group_id,
			'schema.registry.url': self.schema_registry,
			'default.topic.config': {
				'auto.offset.reset': 'smallest'
			}
		})
		_prosumer.subscribe(self.topics)
		return _prosumer

	def start_cmdctrl(self):
		"""
			Method that consumes messages from kafka and performs transaction logic
			@:param: None
			@:returns: None
		"""
		logger.info('Consumer group ID = {}'.format(self.group_id))
		logger.info('Consuming the following topics = {}'.format(self.topics))
		create_thread()
		while self.running:
			try:
				msg = self.consumer.poll(0.001)
				if msg is None:
					continue
				if not msg.error():

					if msg.topic() == 'new-prosumer':
						message = msg.value()
						prosumer_id = message['prosumer_id']
						user_group_id = message['user_group_id']
						facility_id = message['facility_id']
						configuration_id = message['configuration_id']
						user_full_name, user_group_id = UserApi().get_user_full_name(
							user_group_id=user_group_id,
							configuration_id=configuration_id
							)
						facility_name = get_facility_name(facility_id)
						print(f"\n\n\n\tuser_full_name {user_full_name}")
						prosumer = dict(
							prosumer_name=message['prosumer_name'],
							prosumer_url=message['prosumer_url'],
							prosumer_id=prosumer_id,
							probe_names=message.get('probe_names'),
							user_group_id=user_group_id,
							user_full_name=user_full_name,
							configuration_id=message['configuration_id'],
							facility_id=message['facility_id'],
							facility_name=facility_name,
							client_admins=message['client_admins'],
							prosumer_state="NEW PROSUMER",
						)
						logger.info("New Prosumer {}".format(prosumer))
						event = "new-prosumer"
						emit_message(event, prosumer)

						prosumer_url = message['prosumer_url']
						record_prosumer_installation(prosumer_url)
						new_prosumer(message)

					if msg.topic() == 'prosumer-update':
						message = msg.value()
						logger.info(f'Prosumer update message {message}')
						prosumer_id = message['prosumer_id']
						user_group_id = message['user_group_id']
						configuration_id = message['configuration_id']
						user_full_name, user_group_id = UserApi().get_user_full_name(
							user_group_id=user_group_id,
							configuration_id=configuration_id
							)
						facility_name = get_facility_name(facility_id)
						print(f"\n\n\n\tuser_full_name {user_full_name}")
						prosumer = dict(
							prosumer_name=message['prosumer_name'],
							prosumer_url=message['prosumer_url'],
							prosumer_id=prosumer_id,
							probe_names=message.get('probe_names'),
							user_group_id=user_group_id,
							user_full_name=user_full_name,
							configuration_id=message['configuration_id'],
							facility_id=message['facility_id'],
							client_admins=message['client_admins'],
							prosumer_state="NEW PROSUMER",
						)
						event = "prosumer-update"
						logger.info("New Prosumer {}".format(prosumer))

						prosumer_url = message['prosumer_url']
						record_prosumer_installation(prosumer_url)
						emit_message(event, prosumer)
						edit_prosumer(message)

					if msg.topic() == 'delete-prosumer':
						message = msg.value()
						prosumer_id = message['prosumer_id']
						prosumer_url = message['prosumer_url']

						event = 'delete-prosumer'
						emit_message(event, message)

						destroy_prosumer(prosumer_id)
						ignore_prosumer(prosumer_id)
						record_prosumer_uninstallation(prosumer_url)

					if msg.topic() == 'user-update':
						message = msg.value()
						if not isinstance(message, dict):
							continue
						user_group_id = message['user_group_id']
						prosumer_id = message['prosumer_id']
						user_deleted = message['user_deleted']
						if user_deleted:
							unassign_all(user_group_id)
							event = 'unassign-user'
							del message['user_deleted']
							emit_message(event, message)
							continue

						user = UserApi().get_user(user_group_id)
						if not user:
							continue
						user_full_name = f"{user['first_name']} {user['last_name']}"
						message['user_full_name'] = user_full_name
						message['user_group_id'] = user_group_id
						event = 'user-update'
						emit_message(event, prosumer)
						del message['user_deleted']
						update_user(message)

					if msg.topic() == 'new-g0-meter':
						if not isinstance(message, dict):
							continue
						# save_g0(**msg.value())

					if msg.topic() in {'probe-stream', 'gritServer'}:
						message = msg.value()
						if not isinstance(message, dict):
							continue
						if 'data' not in message:
							continue
						self.process_message(message)

				elif msg.error().code() == KafkaError._PARTITION_EOF: # pylint: disable=W0212
					# logger.exception(msg.error())
					continue

				else:
					logger.exception(msg.error())
					continue

			except SerializerError as e:
				logger.exception("Message deserialization failed for {}: {}".format(msg, e))
				# skip serialization errors
				continue
			# sleep(0.00001)
		self.consumer.close()

	@staticmethod
	def get_prosumer_id(probe_id, prosumer):
		source_name = prosumer['sourceName'].upper()
		if 'sourceId' not in prosumer:
			prosumer_id = f"{probe_id}-{source_name}"
		else:
			prosumer_id = prosumer['sourceId']
		return prosumer_id, source_name

	@staticmethod
	def get_power(source_name, power_list):
		measured_power = None
		for prosumer in power_list:
			if prosumer['sourceName'].upper() == source_name:
				measured_power = prosumer['powerSinceLast']
				break
		return measured_power

	@staticmethod
	def get_measured_energy(source_name, measured_energy_list):
		measured_energy = None
		for prosumer in measured_energy_list:
			if prosumer['sourceName'].upper() == source_name:
				measured_energy = prosumer['energySinceLast']
				break
		return measured_energy

	@staticmethod
	def get_energy_today(source_name, energy_today_list):
		energy_today = None
		for prosumer in energy_today_list:
			if prosumer['sourceName'].upper() == source_name:
				energy_today = prosumer['energyToday']
				break
		return energy_today

	@staticmethod
	def get_time_today(source_name, time_today_list):
		time_today = None
		for prosumer in time_today_list:
			if prosumer['sourceName'].upper() == source_name:
				time_today = prosumer['timeToday']
				break
		return time_today


	def process_message(self, message):
		if 'data' in message:
			probe_id = message['master']['id']
			measurement_time = message['master']['time']
			config_ids = message['master']['configuration_IDs']
			from_buffer = is_old_message(measurement_time)
			if from_buffer:
				probe_state = 'BUFFERING'
			else:
				probe_state = 'PROBE ON'
			measured_power_list = message['data'][0]['powerSinceLast']
			measured_energy_list = message['data'][0]['energySinceLast']
			energy_today_list = message['data'][0]['energyTodaySource']
			time_today_list = message['data'][0]['timeTodaySource']
			# list of prosumer names and energy consumed
			# prosumer_queue = deque()
			event = ''
			for prosumer in measured_power_list:
				prosumer_id, source_name = ProsumerMonitor.get_prosumer_id(probe_id, prosumer)
				configuration_id = prosumer['configID_FK']
				prosumer_ = Prosumer.get_one(prosumer_id)
				if not prosumer_:
					event = 'new-prosumer'
					prosumer_ = Prosumer.new(prosumer_id, configurations=config_ids)
				if not is_stale(prosumer_.last_emit_time):
					continue

				measured_power = prosumer['powerSinceLast']
				measured_energy = ProsumerMonitor.get_measured_energy(source_name, measured_energy_list)
				energy_today = ProsumerMonitor.get_energy_today(source_name, energy_today_list)
				time_today = ProsumerMonitor.get_time_today(source_name, time_today_list)

				if from_buffer:
					prosumer_state = 'BUFFERING'
				else:
					if measured_power > 0:
						prosumer_state = 'ON'
					else:
						prosumer_state = 'OFF'

				message = {
					'prosumer_id': prosumer_id,
					'probe_id': probe_id,
					'probe_state': probe_state,
					'prosumer_state': prosumer_state,
					'prosumer_name': source_name,
					'configuration_id': configuration_id,
					'measured_power': f'{float(measured_power):.4f}',
					'measured_energy': f'{float(measured_energy):.4f}',
					'energy_today': f'{float(energy_today):.4f}',
					'time_today': f'{float(time_today):.4f}',
					'last_message': measurement_time,
					'from_buffer': int(from_buffer)
				}
				# prosumer_ = Prosumer.get_one(prosumer_id)
				# if not prosumer_:
				# 	event = 'new-prosumer'
				# 	prosumer_ = Prosumer.new(prosumer_id, configurations=config_ids)
				# else:
				if event != 'new-prosumer':
					event = 'power-update'
				if to_emit(prosumer_, prosumer_state):
					prosumer_.emit()
					emit_message(event, message, prosumer_)
					add_to_queue(record_online_prosumer, message)

				# Prosumer Registration checking.
				# Check if:
				# 1. Prosumer has been assigned to a user.
				# 2. If the facility name of the prosumer is not known
				# 3. If the prosumer has no client admins
				# --------
				# Make api calls to get unavailable data.
					# Refresh probe causes all the prosumers registered to this probe
					# in the prosumer api to be sent to kafka. This is a REST request
					# that triggers an asynchronous action.

					# START FROM HERE
					# ProsumerApi().refresh_probe(probe_id)
			commit()


	def run(self):
		"""
			Method that starts Command and Control
			@:param: None
			@:returns: None
		"""
		logger.info('Starting Command and Control')
		self.start_cmdctrl()
