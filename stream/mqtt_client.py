import paho.mqtt.client as mqtt
import time
import ssl


class MqttClient():

	def __init__(self, mqtt_server_url, mqtt_server_port):
	# def __init__(self, mqtt_server_url, mqtt_server_port, mqtt_cert_name):
		self.mqtt_server_url = mqtt_server_url
		self.mqtt_server_port = int(mqtt_server_port)
		# self.mqtt_cert_name = mqtt_cert_name
		self.client = self.set_up_client()
		self.client.on_connect = self.on_connect
		# self.client.tls_set(self.mqtt_cert_name, cert_reqs=ssl.CERT_NONE, tls_version=ssl.PROTOCOL_TLSv1)
		self.client.connect(
			host=self.mqtt_server_url,
			port=self.mqtt_server_port,
			keepalive=60,
		)
		self.client.loop_start()

	def on_connect(self, client, userdata, flags, rc):
		logger.info("Connected with result code "+str(rc) + "\n")
		logger.info("userdata "+str(userdata) + "\n")
		logger.info("client "+str(client) + "\n")
		logger.info("flags "+str(flags) + "\n")
		logger.info("host " + client.get_host() + "\n")

	def set_up_client(self):
		client = mqtt.Client()
		return client

	@property
	def get_client(self):
		return self.client
