from api.models import StreamDB
from os import getenv
import requests
from logger import logger


class ProsumerUpdate(object):
	def __init__(self, message, prosumer_monitor):
		self.message = message
		self.db = StreamDB(prosumer_monitor)

	def user_full_name(self, user_group_id):
		cookies = {'role_id': '6'}
		user = requests.get(getenv('GROUP_USER_URI').format(group_id=user_group_id), cookies=cookies)
		if user.status_code == 200:
			user_ = user.json()['data']
			first_name = user_.get('first_name')
			last_name = user_.get('last_name')
			full_name = '{} {}'.format(first_name, last_name)
			return full_name
		return ''

	def get_facility_name(self, facility_id):
		cookies = {'role_id': '6'}
		if facility_id:
			facility = requests.get(getenv('FACILITY_NAME_URI').format(facility_id=facility_id), cookies=cookies)
			if facility.status_code == 200:
				facility_ = facility.json()
				facility_name = facility_['data'].get('facility_name')
				return facility_name
		return ''

	# def destroy_prosumer(self, prosumer_name, facility_id):
	# 	cookies = {'role_id': '6'}

	# 	if all([prosumer_name, facility_id]):
	# 		facility = requests.delete(getenv('PROSUMER_DESTROY_URI').format(prosumer_name, facility_id), cookies=cookies)
	# 		if facility.status_code == 200:
	# 			facility_ = facility.json()
	# 			return facility_
	# 	return ''

	def perform_update(self):
		user_group_id = self.message.get('user_group_id')
		to_delete = False
		try:
			to_delete = self.message['to_delete']
			del self.message['to_delete']
		except KeyError as e:
			logger.exception(e)
		facility_id = self.message.get('facility_id')
		prosumer_name = self.message.get('prosumer_name')
		client_admins = self.message.get('client_admins')
		was_assigned = self.message.get('was_assigned')
		del self.message['was_assigned']
		prosumer = self.db.get_prosumer(prosumer_name)
		event = 'prosumer_update'
		to_emit = False
		"""
			Simplify the database inserts to use just one insert per operation.
			This will improve the speed of the update.
		"""
		if to_delete:
			result = self.db.delete_prosumer(prosumer_name)
			if result.acknowledged:
				self.destroy_prosumer(prosumer_name, facility_id)
			return event, to_emit, prosumer

		if not prosumer:
			# NEW METER, nothing to update
			prosumer_state = 'NEW METER'
			self.message['prosumer_state'] = prosumer_state
			prosumer = self.db.register_prosumer(**self.message)
			if prosumer.acknowledged:
				prosumer = self.db.get_prosumer(prosumer_name)
				to_emit = True
				event = 'new_prosumer'
			# return None, to_emit, prosumer

		if user_group_id:
			same_user_group = (user_group_id == prosumer.get('user_group_id'))
			if not same_user_group:
				# If the prosumer was newly assigned to a user.
				# Or if the prosumer was reassigned to a user.
				prosumer_user = self.user_full_name(user_group_id)
				prosumer_state = 'NEW USER'
				self.db.update_state(prosumer_name, prosumer_state)
				prosumer = self.db.update_prosumer_user(prosumer_name, prosumer_user, user_group_id)
				logger.info("USER GROUP UPDATE FROM PROSUMER = {}".format(prosumer))
				to_emit = True

		if not user_group_id:
			prosumer_user = 'UNASSIGNED'
			user_group_id = None
			prosumer = self.db.update_prosumer_user(prosumer_name, prosumer_user, user_group_id)
			if was_assigned:
				prosumer_state = 'UNASSIGNED'
				prosumer = self.db.update_state(prosumer_name, prosumer_state)
				logger.info("USER GROUP UPDATE FROM PROSUMER = {}".format(prosumer))
				to_emit = True

		if facility_id:
			if facility_id != prosumer.get('facility_id') or prosumer.get('facility_name') is None:
				# If the prosumer was newly assigned to a facility.
				# Or if the prosumer was reassigned to a facility.
				facility_name = self.get_facility_name(facility_id)
				prosumer_state = 'NEW METER'
				self.db.update_state(prosumer_name, prosumer_state)
				prosumer = self.db.update_prosumer_facility(prosumer_name, facility_name, facility_id)
				to_emit = True

		if client_admins:
			if not client_admins == prosumer.get('client_admins'):
				prosumer_state = 'NEW CLIENT ADMIN'
				self.db.update_state(prosumer_name, prosumer_state)
				prosumer = self.db.update_client_admins(prosumer_name, client_admins)
				to_emit = True
		prosumer = self.db.get_prosumer(prosumer_name=prosumer_name)
		logger.info('Update Complete - {}'.format(prosumer))
		return event, to_emit, prosumer
