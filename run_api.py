from eventlet import monkey_patch
monkey_patch() # Here so that socketio will work
from flask import Response, request

from api import db
from api.app_object import app  # Imported here so that gunicorn will find it


with app.app_context():
	db.create_all()


@app.after_request
def verify(response):
	if request.method == 'OPTIONS':
		# Hack to prevent CORS errors
		# Please look into this as the original authors aren't too sure what is going on here
		resp = {}
		resp['success'] = True
		resp['message'] = 'DON\'T use OPTIONS in any of the app routes'
		response = Response(resp, 200, {})
	return response


@app.teardown_appcontext
def shutdown_session(exception=None):
	db.session.commit()
	db.session.remove()
