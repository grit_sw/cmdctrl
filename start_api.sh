#!/bin/sh

source venv/bin/activate

export FLASK_APP=run_api.py
# exec flask api
exec gunicorn -b :5000 -k eventlet --access-logfile - --error-logfile - run_api:app
